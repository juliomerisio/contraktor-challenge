import React from 'react';
import { RelayEnvironmentProvider } from 'react-relay/hooks';
import Environment from './relay/Environment';
import ErrorBoundary from './ErrorBoundary';

const Providers: React.FC = ({ children }) => {
  return (
    <RelayEnvironmentProvider environment={Environment}>
      <ErrorBoundary>{children} </ErrorBoundary>
    </RelayEnvironmentProvider>
  );
};

export default Providers;
