import React from 'react';
import styled from 'styled-components';
import graphql from 'babel-plugin-relay/macro';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Menu from '../components/Menu';

const { useLazyLoadQuery } = require('react-relay/hooks');


const Container = styled.div`
  display: flex;
  width: 100%;
`;
const Wrapper = styled.div`
  max-width: 800px;
  width: 100%;
  display: flex;
  padding: 50px;
  flex-direction: column;
`;
const Card = styled(Link)`
  display: flex;
  margin-bottom: 25px;
  flex-direction: column;
  border: 1px solid #ccc;
  padding: 20px;
  border-radius: 4px;
  background: #fff;
`;

const NodeQuery = graphql`
  query HomeQuery {
    contracts {
      id
      title
      validityStart
      validityEnd
    }
  }
`;
const UsersQuery = graphql`
  query HomeUsersQuery {
    users {
      name
      email
      id
    }
  }
`;
const Home = () => {
  return (
    <Container>
      <Menu />
      <Wrapper>
        <React.Suspense fallback={<h1>Carregando contratos</h1>}>
          <Content />
        </React.Suspense>
        <React.Suspense fallback={<h1>Carregando usuários</h1>}>
          <ContentUser />
        </React.Suspense>
      </Wrapper>
    </Container>
  );
};

const Content = () => {

  const { contracts } = useLazyLoadQuery(
    NodeQuery,
    {},
    { fetchPolicy: 'store-and-network' }
  );
  const formatDate = React.useCallback((date: string) => {
    return moment(date).format('DD/MM/YYYY');
  }, []);


  return (
    <div>
      <h1>{contracts?.length > 0 && 'Contratos'}</h1>
      {contracts?.map((contract: any) => (
        <Card key={contract?.title} to={`/contratos/${contract?.id}`}>
          <strong>{contract?.title}</strong>
          <small>
            {formatDate(contract?.validityStart)} ~{' '}
            {formatDate(contract?.validityEnd)}
          </small>
        </Card>
      ))}
    </div>
  );
};



const ContentUser = () => {
  const { users } = useLazyLoadQuery(
    UsersQuery,
    {},
    { fetchPolicy: 'store-and-network' }
  );

  return (
    <div>
      <h1>{users?.length > 0 && 'Usuários'}</h1>
      {users?.map((user: any) => (
        <Card key={user?.id} to={`/pessoas/${user?.id}`}>
          <strong>{user?.name}</strong>
          <small>{user?.email}</small>
        </Card>
      ))}
    </div>
  );
};

export default Home;
