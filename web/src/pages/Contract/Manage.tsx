import React from 'react';
import styled from 'styled-components';
import { useFormik } from 'formik';
import * as yup from 'yup';
import graphql from 'babel-plugin-relay/macro';
import { useMutation } from 'react-relay/lib/relay-experimental';
import { message } from 'antd';
import { useHistory, useParams } from 'react-router-dom';

import moment from 'moment';
import docDefault from '../../assets/doc_placeholder.jpg';
import Input from '../../components/Input';
import { InputRange } from '../../components/InputRange';
import InputCard from '../../components/InputCard';
import Card from '../../components/Card';
import { Button } from '../../components/Button';
import Modal from '../../components/Modal';
import Layout from '../../layout/Default';
import { SearchUser } from './SearchUser';
import { InputUpload } from '../../components/InputUpload';
import { UpdateContractInput } from './__generated__/ManageContractUpdateMutation.graphql';
import { DeleteContractInput } from './__generated__/ManageDeleteMutation.graphql';
import { CreateContractInput } from './__generated__/ManageCreateContractMutation.graphql';

const { useLazyLoadQuery } = require('react-relay/hooks');

const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;
const Content = styled.div`
  margin-top: 100px;
  max-width: 550px;
  width: 100%;
  h1 {
    display: flex;
    justify-content: space-between;
    button {
      background: #e91e63;
    }
  }
`;
const Form = styled.form`
  display: flex;
  flex-direction: column;
  button {
    align-self: flex-end;
    margin: 20px 0;
  }
`;
const Info = styled.div`
  display: flex;
  align-items: center;
  > div:nth-child(2) {
    width: auto;
  }
`;
const PartsContainer = styled.div`
  margin-top: 88px;
  display: grid;
  grid-template-columns: repeat(2, minmax(250px, 1fr));
  grid-gap: 15px;
`;
const InputContainer = styled.div`
  width: 100%;
`;

const SearchUserContainer = styled.div`
  max-width: 1011px;
  width: 100%;
  height: 100%;
  background: #fff;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
const ModalFooter = styled.div`
  height: 86px;
  background: #fbfbfd;
  border-top: 1px solid #ecedf6;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 0 58px;
`;

const validation = yup.object().shape({
  title: yup.string().required('O título do contrato é obrigatório'),
  validity: yup
    .array()
    .of(yup.string())
    .required('Selecione a data de vigência do contrato'),
  user_id: yup
    .array()
    .of(yup.string().required())
    .required('As partes do contrato são obrigatórias'),
  document_id: yup.string().required('O Arquivo é obrigatório'),
});

const REGISTER_CONTRACT = graphql`
  mutation ManageCreateContractMutation($input: CreateContractInput!) {
    createContract(input: $input) {
      contract {
        _id
        id
        title
        validityStart
        validityEnd
        parts_ids
        document {
          url
        }
      }
    }
  }
`;
const UPDATE_CONTRACT = graphql`
  mutation ManageContractUpdateMutation($input: UpdateContractInput!) {
    updateContract(input: $input) {
      contract {
        _id
        id
        title
        validityStart
        validityEnd
        parts_ids
        document {
          url
        }
      }
    }
  }
`;

const NodeQuery = graphql`
  query ManageContractNodeQuery($id: ID!) {
    node(id: $id) {
      ... on Contract {
        id
        _id
        title
        validityStart
        validityEnd
        document {
          id
          _id
          url
        }
        parts {
          _id
          id
          name
          email
          avatar {
            url
          }
        }
      }
    }
  }
`;
const DELETE_CONTRACT = graphql`
  mutation ManageDeleteMutation($input: DeleteContractInput!) {
    deleteContract(input: $input) {
      message
    }
  }
`;

const CreateContract = () => {
  return (
    <Layout>
      <React.Suspense fallback={<h1>loading..</h1>}>
        <FormContract />
      </React.Suspense>
    </Layout>
  );
};

const FormContract = () => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [commit] = useMutation<CreateContractInput>(REGISTER_CONTRACT);
  const [updateMutation] = useMutation<UpdateContractInput>(UPDATE_CONTRACT);
  const [onDeleteMutation] = useMutation<DeleteContractInput>(DELETE_CONTRACT);
  const history = useHistory();
  const params = useParams<{ id: string }>();

  const { node: contract } = useLazyLoadQuery(
    NodeQuery,
    { id: params?.id || '2' },
    { fetchPolicy: 'store-or-network' }
  );
  const isUpdate = !!contract?.id;

  const [imgPreview, setImgPreview] = React.useState<any>(
    () => isUpdate ? docDefault : ''
  );

  const onDelete = React.useCallback(async () => {
    onDeleteMutation({
      variables: {
        input: {
          id: contract?._id,
        },
      },
      onCompleted: () => {
        history.push('/');
        message.success('Contrato deletado com sucesso');
      },
    });
  }, [contract, history, onDeleteMutation]);

  const onSubmit = React.useCallback(
    async (values: any) => {
      const { validity, ...cleanValues } = values;
      commit({
        variables: {
          input: {
            values: {
              ...cleanValues,
              validityStart: validity[0]?.format(),
              validityEnd: validity[1]?.format(),
              user_id: values.user_id.map((el: any) => el._id),
            },
          },
        },

        onCompleted: ({ createContract }: any) => {
          const Errors = createContract?.contract?.Error;
          if (!Errors) {
            resetForm(); //eslint-disable-line
            setImgPreview(undefined);
            message.success('Contrato cadastrado com sucesso!');
            history.push(`/contratos/${createContract.contract?.id}`);
          }
          return Errors?.forEach((error: string) => message.error(error));
        },
      });
    },
    [commit] //eslint-disable-line
  );
  const onUpdate = React.useCallback(
    async (values: any) => {
      const { validity, ...cleanValues } = values;
      updateMutation({
        variables: {
          input: {
            id: contract?._id,
            values: {
              ...cleanValues,
              validityStart: validity[0]?.format(),
              validityEnd: validity[1]?.format(),
              user_id: values.user_id.map((el: any) => el._id),
            },
          },
        },

        onCompleted: ({ updateContract }: any) => {
          const Errors = updateContract?.contract?.Error;
          if (!Errors) {
            message.success('Contrato editado com sucesso!');
            history.push(`/contratos/${updateContract.contract?.id}`);          }
          return Errors?.forEach((error: string) => message.error(error));
        },
      });
    },
    [commit] //eslint-disable-line
  );

  const {
    handleSubmit,
    handleChange,
    values,
    setFieldValue,
    touched,
    errors,
    resetForm,
  } = useFormik({
    initialValues: {
      title: contract?.title || '',
      validity: contract?.validityStart
        ? [moment(contract?.validityStart), moment(contract?.validityEnd)]
        : ([] as any[]),
      user_id: (contract?.parts as any[]) || ([] as any[]),
      document_id: (contract?.document?._id as string) || '',
    },
    onSubmit: isUpdate ? onUpdate : onSubmit,
    validationSchema: validation,
  });

  return (
    <Container>
      <Content>
        <h1>
          {isUpdate ? 'Edição de contrato' : 'Cadastro de contrato'}
          {isUpdate && <Button onClick={() => onDelete()}>Deletar</Button>}
        </h1>
        <Form onSubmit={handleSubmit}>
          <Info>
            <InputContainer>
              <Input
                name="title"
                onChange={handleChange}
                value={values.title}
                label="Título"
                touched={touched.title}
                error={errors.title}
              />
              <InputRange
                error={errors.validity}
                touched={touched.validity}
                name="validity"
                value={values.validity}
                label="Periodo de vigência"
                setFieldValue={setFieldValue}
              />
            </InputContainer>

            <InputUpload
              error={errors.document_id}
              touched={touched.document_id}
              setFieldValue={setFieldValue}
              setUrl={setImgPreview}
              url={imgPreview}
              name="document_id"
            />
          </Info>

          <PartsContainer>
            <InputCard onClick={() => setIsOpen(!isOpen)} />
            {values.user_id.map(item => (
              <Card
                key={item.id}
                data={item}
                allowDelete
                onRemove={() => {
                  const removeItem = values.user_id.filter(
                    el => el.id !== item?.id
                  );
                  return setFieldValue('user_id', removeItem);
                }}
              />
            ))}
          </PartsContainer>
          <Button>{isUpdate ? 'Salvar' : 'Cadastrar'}</Button>
        </Form>
      </Content>
      <Modal open={isOpen} closeAction={() => setIsOpen(false)}>
        <SearchUserContainer>
          <SearchUser parts={values.user_id} setFieldValue={setFieldValue} />
          <ModalFooter>
            <Button onClick={() => setIsOpen(false)}>Adicionar</Button>
          </ModalFooter>
        </SearchUserContainer>
      </Modal>
    </Container>
  );
};

export default CreateContract;
