/* tslint:disable */
/* eslint-disable */

import { ConcreteRequest } from "relay-runtime";
export type DeleteContractInput = {
    id: string;
};
export type ManageDeleteMutationVariables = {
    input: DeleteContractInput;
};
export type ManageDeleteMutationResponse = {
    readonly deleteContract: {
        readonly message: string | null;
    } | null;
};
export type ManageDeleteMutation = {
    readonly response: ManageDeleteMutationResponse;
    readonly variables: ManageDeleteMutationVariables;
};



/*
mutation ManageDeleteMutation(
  $input: DeleteContractInput!
) {
  deleteContract(input: $input) {
    message
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input",
    "type": "DeleteContractInput!"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "DeleteContractPayload",
    "kind": "LinkedField",
    "name": "deleteContract",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "message",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ManageDeleteMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ManageDeleteMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "id": null,
    "metadata": {},
    "name": "ManageDeleteMutation",
    "operationKind": "mutation",
    "text": "mutation ManageDeleteMutation(\n  $input: DeleteContractInput!\n) {\n  deleteContract(input: $input) {\n    message\n  }\n}\n"
  }
};
})();
(node as any).hash = 'b9d1e985d46947a832bcd52afac7bc4b';
export default node;
