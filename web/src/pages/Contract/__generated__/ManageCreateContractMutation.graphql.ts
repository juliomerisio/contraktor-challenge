/* tslint:disable */
/* eslint-disable */

import { ConcreteRequest } from "relay-runtime";
export type CreateContractInput = {
    clientMutationId?: string | null;
    values: CreateContractValuesInput;
};
export type CreateContractValuesInput = {
    title: string;
    validityStart: string;
    validityEnd: string;
    document_id: string;
    user_id?: Array<string> | null;
};
export type ManageCreateContractMutationVariables = {
    input: CreateContractInput;
};
export type ManageCreateContractMutationResponse = {
    readonly createContract: {
        readonly contract: {
            readonly _id: string;
            readonly id: string;
            readonly title: string;
            readonly validityStart: string;
            readonly validityEnd: string;
            readonly parts_ids: ReadonlyArray<string | null> | null;
            readonly document: {
                readonly url: string | null;
            } | null;
        } | null;
    };
};
export type ManageCreateContractMutation = {
    readonly response: ManageCreateContractMutationResponse;
    readonly variables: ManageCreateContractMutationVariables;
};



/*
mutation ManageCreateContractMutation(
  $input: CreateContractInput!
) {
  createContract(input: $input) {
    contract {
      _id
      id
      title
      validityStart
      validityEnd
      parts_ids
      document {
        url
        id
      }
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateContractInput!"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "_id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "title",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "validityStart",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "validityEnd",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "parts_ids",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "url",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ManageCreateContractMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "CreateContractPayload",
        "kind": "LinkedField",
        "name": "createContract",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Contract",
            "kind": "LinkedField",
            "name": "contract",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              (v6/*: any*/),
              (v7/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Avatar",
                "kind": "LinkedField",
                "name": "document",
                "plural": false,
                "selections": [
                  (v8/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ManageCreateContractMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "CreateContractPayload",
        "kind": "LinkedField",
        "name": "createContract",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Contract",
            "kind": "LinkedField",
            "name": "contract",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              (v6/*: any*/),
              (v7/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Avatar",
                "kind": "LinkedField",
                "name": "document",
                "plural": false,
                "selections": [
                  (v8/*: any*/),
                  (v3/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "id": null,
    "metadata": {},
    "name": "ManageCreateContractMutation",
    "operationKind": "mutation",
    "text": "mutation ManageCreateContractMutation(\n  $input: CreateContractInput!\n) {\n  createContract(input: $input) {\n    contract {\n      _id\n      id\n      title\n      validityStart\n      validityEnd\n      parts_ids\n      document {\n        url\n        id\n      }\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = '3fcd041f9bef4018a15e839ae2d84644';
export default node;
