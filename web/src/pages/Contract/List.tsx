import React from 'react';
import styled from 'styled-components';
import { useParams, Link } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import FileViewer from 'react-file-viewer';
import moment from 'moment';
import Card from '../../components/Card';

const { useLazyLoadQuery } = require('react-relay/hooks');

const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100vh;
`;
const Parts = styled.div`
  width: 377px;
  height: 100vh;
  overflow-y: scroll;
  border-left: 1px solid #ecedf6;
  background: #fff;
  padding: 15px;
  display: flex;
  flex-direction: column;
  a {
    font-size: 14px;
    margin-left: 15px;
  }
  h1 {
    margin-bottom: 14px;
  }
  small {
    margin-bottom: 50px;
  }
  > div {
    margin-bottom: 20px;
  }
  ::-webkit-scrollbar {
    display: none;
  }
`;
const Content = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  .pg-viewer-wrapper::-webkit-scrollbar {
    display: none;
  }
  #docx {
    margin-top: 100px;
  }
`;
const Document = styled(FileViewer)`
  max-width: 877px;
  width: 100%;
  background: #fff;
  height: 893px;
`;
const PdfRender = styled.iframe`
  max-width: 877px;
  width: 100%;
  background: #fff;
  height: 893px;
`;

const NodeQuery = graphql`
  query ListContractNodeQuery($id: ID!) {
    node(id: $id) {
      ... on Contract {
        id
        title
        validityStart
        validityEnd
        document {
          url
        }
        parts {
          name
          email
          avatar {
            url
          }
        }
      }
    }
  }
`;

const List = () => {
  return (
    <Container>
      <React.Suspense fallback={<h1>Loading...</h1>}>
        <ContractInfo />
      </React.Suspense>
    </Container>
  );
};

const ContractInfo = () => {
  const params = useParams<{ id: string }>();

  const { node: contract } = useLazyLoadQuery(
    NodeQuery,
    { id: params?.id || '2' },
    { fetchPolicy: 'store-and-network' }
  );
  const extension = contract?.document?.url?.split('.').pop();
  const formatDate = React.useCallback((date: string) => {
    return moment(date).format('DD/MM/YYYY');
  }, []);

  if(!contract?.id) {
    return (
      <Content>Contrato não encontrado</Content>
    )
  }

  return (
    <>
      <Content>
        {extension === 'pdf' ? (
          <PdfRender src={contract?.document?.url} />
        ) : (
          <Document
            fileType={extension}
            filePath={contract?.document?.url}
            errorComponent={<h1>Não foi possível encontrar o arquivo</h1>}
          />
        )}
      </Content>
      <Parts>
        <h1>
          {contract?.title}{' '}
          <Link to={`/contratos/editar/${contract?.id}`}>Editar</Link>{' '}
        </h1>
        <small>
          {formatDate(contract?.validityStart)} a{' '}
          {formatDate(contract?.validityEnd)}
        </small>
        {contract?.parts?.map((item: any) => (
          <Card key={item?.email} data={item} />
        ))}
      </Parts>
    </>
  );
};

export default List;
