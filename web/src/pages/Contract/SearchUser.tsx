import React from 'react';
import styled from 'styled-components';
import graphql from 'babel-plugin-relay/macro';
import InputSearch from '../../components/InputSearch';
import Card from '../../components/Card';

const { useLazyLoadQuery } = require('react-relay/hooks');

const ModalHeader = styled.div`
  height: 98px;
  background: #fbfbfd;
  border-bottom: 1px solid #ecedf6;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 58px;
  p {
    margin: 0;
  }
`;

const ModalContent = styled.div`
  overflow-y: scroll;
  height: 100%;
  justify-content: center;
  padding: 30px 0px;
  display: flex;
  flex-wrap: wrap;
  background: #f6f6fa;
  &::-webkit-scrollbar {
    display: none;
  }
  > div {
    margin: 10px;
  }
`;

const UsersQuery = graphql`
  query SearchUserQuery($q: String) {
    users(q: $q) {
      ... on User {
        _id
        id
        name
        cpf
        surname
        phone
        email
        avatar {
          path
          url
          _id
        }
      }
    }
  }
`;
interface Props {
  setFieldValue: (a: string, b: any[]) => void;
  parts: any;
}

export const SearchUser = ({ setFieldValue, parts }: Props) => {
  const [query, setQuery] = React.useState<string | undefined>(undefined);

  let time: number = 0;
  const handleSearch = (value: string) => {
    clearTimeout(time);
    // @ts-ignore
    time = setTimeout(() => {
      setQuery(value);
    }, 300);
  };
  return (
    <>
      <ModalHeader>
        <InputSearch onChange={e => handleSearch(e.target.value)} />
        <p>
          {parts.length}{' '}
          {parts.length > 1 ? 'Usuários selecionados' : 'Usuário selecionado'}
        </p>
      </ModalHeader>
      <React.Suspense fallback={<div />}>
        <Content query={query} setFieldValue={setFieldValue} parts={parts} />
      </React.Suspense>
    </>
  );
};

interface ContentProps {
  setFieldValue: (a: string, b: any) => void;
  query?: string;
  parts: any;
}
const Content = ({ query, setFieldValue, parts }: ContentProps) => {
  const { users } = useLazyLoadQuery(
    UsersQuery,
    { q: query },
    { fetchPolicy: 'store-and-network' }
  );

  return (
    <ModalContent>
      {users?.map((item: any) => (
        <Card
          parts={parts}
          key={item.id}
          allowSelect
          data={item}
          onAdd={() => {
            const isInArray = parts.find((el: any) => el.id === item.id);
            const removeItem = parts.filter(
              (el: any) => el.id !== isInArray?.id
            );
            if (isInArray) {
              return setFieldValue('user_id', removeItem);
            }
            return setFieldValue('user_id', [...parts, item]);
          }}
        />
      ))}
    </ModalContent>
  );
};
