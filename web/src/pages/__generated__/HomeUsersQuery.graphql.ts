/* tslint:disable */
/* eslint-disable */

import { ConcreteRequest } from "relay-runtime";
export type HomeUsersQueryVariables = {};
export type HomeUsersQueryResponse = {
    readonly users: ReadonlyArray<{
        readonly name: string;
        readonly email: unknown;
        readonly id: string;
    } | null> | null;
};
export type HomeUsersQuery = {
    readonly response: HomeUsersQueryResponse;
    readonly variables: HomeUsersQueryVariables;
};



/*
query HomeUsersQuery {
  users {
    name
    email
    id
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "User",
    "kind": "LinkedField",
    "name": "users",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "name",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "email",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "HomeUsersQuery",
    "selections": (v0/*: any*/),
    "type": "Query"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "HomeUsersQuery",
    "selections": (v0/*: any*/)
  },
  "params": {
    "id": null,
    "metadata": {},
    "name": "HomeUsersQuery",
    "operationKind": "query",
    "text": "query HomeUsersQuery {\n  users {\n    name\n    email\n    id\n  }\n}\n"
  }
};
})();
(node as any).hash = '1d5d4536b0ddad867959aae1f4938caf';
export default node;
