/* tslint:disable */
/* eslint-disable */

import { ConcreteRequest } from "relay-runtime";
export type CreateUserInput = {
    clientMutationId?: string | null;
    values: CreateUserValuesInput;
};
export type CreateUserValuesInput = {
    name: string;
    cpf: string;
    surname: string;
    phone: string;
    avatar_id?: string | null;
    email: unknown;
};
export type FormUserCreateMutationVariables = {
    input: CreateUserInput;
};
export type FormUserCreateMutationResponse = {
    readonly createUser: {
        readonly user: {
            readonly name: string;
        } | null;
        readonly Error: ReadonlyArray<string> | null;
        readonly clientMutationId: string | null;
    };
};
export type FormUserCreateMutation = {
    readonly response: FormUserCreateMutationResponse;
    readonly variables: FormUserCreateMutationVariables;
};



/*
mutation FormUserCreateMutation(
  $input: CreateUserInput!
) {
  createUser(input: $input) {
    user {
      name
      id
    }
    Error
    clientMutationId
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateUserInput!"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "Error",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "clientMutationId",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "FormUserCreateMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "CreateUserPayload",
        "kind": "LinkedField",
        "name": "createUser",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "user",
            "plural": false,
            "selections": [
              (v2/*: any*/)
            ],
            "storageKey": null
          },
          (v3/*: any*/),
          (v4/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "FormUserCreateMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "CreateUserPayload",
        "kind": "LinkedField",
        "name": "createUser",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "user",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          (v3/*: any*/),
          (v4/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "id": null,
    "metadata": {},
    "name": "FormUserCreateMutation",
    "operationKind": "mutation",
    "text": "mutation FormUserCreateMutation(\n  $input: CreateUserInput!\n) {\n  createUser(input: $input) {\n    user {\n      name\n      id\n    }\n    Error\n    clientMutationId\n  }\n}\n"
  }
};
})();
(node as any).hash = '409335d8fd4a47c67ab01fbdc9c1b08e';
export default node;
