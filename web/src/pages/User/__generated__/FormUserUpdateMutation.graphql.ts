/* tslint:disable */
/* eslint-disable */

import { ConcreteRequest } from "relay-runtime";
export type UpdateUserInput = {
    clientMutationId?: string | null;
    id: string;
    values: UpdateUserValuesInput;
};
export type UpdateUserValuesInput = {
    name?: string | null;
    cpf?: string | null;
    avatar_id?: string | null;
    surname?: string | null;
    phone?: string | null;
    email?: unknown | null;
};
export type FormUserUpdateMutationVariables = {
    input: UpdateUserInput;
};
export type FormUserUpdateMutationResponse = {
    readonly updateUser: {
        readonly user: {
            readonly name: string;
        } | null;
        readonly Error: ReadonlyArray<string> | null;
        readonly clientMutationId: string | null;
    } | null;
};
export type FormUserUpdateMutation = {
    readonly response: FormUserUpdateMutationResponse;
    readonly variables: FormUserUpdateMutationVariables;
};



/*
mutation FormUserUpdateMutation(
  $input: UpdateUserInput!
) {
  updateUser(input: $input) {
    user {
      name
      id
    }
    Error
    clientMutationId
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input",
    "type": "UpdateUserInput!"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "Error",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "clientMutationId",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "FormUserUpdateMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "UpdateUserPayload",
        "kind": "LinkedField",
        "name": "updateUser",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "user",
            "plural": false,
            "selections": [
              (v2/*: any*/)
            ],
            "storageKey": null
          },
          (v3/*: any*/),
          (v4/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "FormUserUpdateMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "UpdateUserPayload",
        "kind": "LinkedField",
        "name": "updateUser",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "user",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          (v3/*: any*/),
          (v4/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "id": null,
    "metadata": {},
    "name": "FormUserUpdateMutation",
    "operationKind": "mutation",
    "text": "mutation FormUserUpdateMutation(\n  $input: UpdateUserInput!\n) {\n  updateUser(input: $input) {\n    user {\n      name\n      id\n    }\n    Error\n    clientMutationId\n  }\n}\n"
  }
};
})();
(node as any).hash = '5094c9c6c098930b22dd6c85abbc933c';
export default node;
