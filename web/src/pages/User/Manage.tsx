import React from 'react';
import styled from 'styled-components';
import { Form } from './Form';
import Layout from '../../layout/Default';

const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;
const Content = styled.div`
  margin-top: 100px;
  max-width: 787px;
  width: 100%;
  h1 {
    margin-bottom: 69px;
  }
`;

const Loading = () => {
  return <h1>Loading...</h1>;
};

const UserForm: React.FC = () => {
  return (
    <Layout>
      <Container>
        <Content>
          <React.Suspense fallback={<Loading />}>
            <Form />
          </React.Suspense>
        </Content>
      </Container>
    </Layout>
  );
};

export default UserForm;
