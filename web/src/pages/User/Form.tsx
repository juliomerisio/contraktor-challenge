import React from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useMutation } from 'react-relay/lib/relay-experimental';
import * as yup from 'yup';

import { message } from 'antd';
import styled from 'styled-components';
import { useParams, useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import Input from '../../components/Input';
import { Button } from '../../components/Button';
import { InputUpload } from '../../components/InputUpload';
import { ManageCreateContractMutation } from '../Contract/__generated__/ManageCreateContractMutation.graphql';

const { useLazyLoadQuery } = require('react-relay/hooks');

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  button {
    align-self: flex-end;
    margin-top: 20px;
  }
`;
const Row = styled.div`
  display: flex;
  > div:first-of-type {
    margin-right: 15px;
  }
`;
const Empty = styled.div`
  width: 100%;
`;
const REGISTER_USER = graphql`
  mutation FormUserCreateMutation($input: CreateUserInput!) {
    createUser(input: $input) {
      user {
        name
      }
      Error
      clientMutationId
    }
  }
`;
const UPDATE_USER = graphql`
  mutation FormUserUpdateMutation($input: UpdateUserInput!) {
    updateUser(input: $input) {
      user {
        name
      }
      Error
      clientMutationId
    }
  }
`;
const NodeQuery = graphql`
  query FormUserNodeQuery($id: ID!) {
    node(id: $id) {
      ... on User {
        _id
        id
        name
        cpf
        surname
        phone
        email
        avatar {
          path
          url
          _id
        }
      }
    }
  }
`;
export const Form = () => {
  const params = useParams<{ id: string }>();
  const history = useHistory()

  const { node: user } = useLazyLoadQuery(
    NodeQuery,
    { id: params?.id || '2' },
    { fetchPolicy: 'store-and-network' }
  );
  const [imgPreview, setImgPreview] = React.useState(user?.avatar?.url);
  const [commit] = useMutation<ManageCreateContractMutation>(REGISTER_USER);
  const [commitUpdate] = useMutation(UPDATE_USER);

  const onSubmit = React.useCallback(
    async (values: any) => {
      commit({
        variables: {
          input: {
            values,
          },
        },

        onCompleted: ({ createUser }: any) => {
          const Errors = createUser?.Error;
          const hasErrors = createUser?.Error?.length > 0;
          if (!hasErrors) {
            resetForm(); //eslint-disable-line
            message.success('Usuário cadastrado com sucesso');
            history.push('/')
            return setImgPreview('');
          }
          return Errors.forEach((error: string) => message.error(error));
        },
      });
    },
    [commit] //eslint-disable-line
  );

  const isUpdate = !!user?.id;
  const onUpdate = React.useCallback(
    async (values: any) => {
      commitUpdate({
        variables: {
          input: {
            id: user?._id,
            values,
          },
        },

        onCompleted: ({ updateUser }: any) => {
          const Errors = updateUser?.Error;
          const hasErrors = updateUser?.Error?.length > 0;
          if (!hasErrors) {
            message.success('Usuário alterado sucesso');
          }
          return Errors?.forEach((error: string) => message.error(error));
        },
      });
    },
    [commitUpdate, user]
  );

  const validation = yup.object().shape({
    name: yup.string().required('O Nome é obrigatório'),
    surname: yup.string().required('O Sobrenome é obrigatório'),
    cpf: yup.string().required('O Cpf é obrigatório'),
    email: yup
      .string()
      .min(13)
      .email('Insira um email válido')
      .required('O Email é obrigatório'),
    phone: yup
      .string()
      .min(13)
      .required('O telefone é obrigatório'),
    avatar_id: yup.string().nullable(),
  });

  const {
    handleSubmit,
    handleChange,
    values,
    touched,
    errors,
    setFieldValue,
    resetForm,
  } = useFormik({
    initialValues: {
      name: user?.name || '',
      surname: user?.surname || '',
      cpf: user?.cpf || '',
      email: user?.email || '',
      phone: user?.phone || '',
      avatar_id: user?.avatar?._id,
    },
    onSubmit: isUpdate ? onUpdate : onSubmit,
    validationSchema: validation,
  });

  return (
    <>
      <h1>{user?._id ? `Edição de Usuário` : `Cadastro de usuário`}</h1>
      <InputUpload
        setUrl={setImgPreview}
        url={imgPreview}
        setFieldValue={setFieldValue}
      />
      <FormContainer onSubmit={handleSubmit}>
        <Row>
          <Input
            name="name"
            onChange={handleChange}
            value={values.name}
            label="Nome"
            touched={touched.name}
            error={errors.name}
          />
          <Input
            name="surname"
            onChange={handleChange}
            value={values.surname}
            label="Sobrenome"
            touched={touched.surname}
            error={errors.surname}
          />
        </Row>
        <Row>
          <Input
            name="cpf"
            onChange={handleChange}
            value={values.cpf}
            label="CPF"
            touched={touched.cpf}
            error={errors.cpf}
            validation="cpf"
          />
          <Input
            name="email"
            onChange={handleChange}
            value={values.email}
            label="Email"
            touched={touched.email}
            error={errors.email}
          />
        </Row>
        <Row>
          <Input
            name="phone"
            onChange={handleChange}
            value={values.phone}
            label="Telefone"
            touched={touched.phone}
            error={errors.phone}
            validation="phone"
          />
          <Empty />
        </Row>
        <Button>{isUpdate ? 'Salvar' : 'Cadastrar'}</Button>
      </FormContainer>
    </>
  );
};
