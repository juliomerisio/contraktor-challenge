import React from 'react';
import axios from 'axios';
import styled from 'styled-components';
import { FiUploadCloud } from 'react-icons/fi';
import { message } from 'antd';
import docDefault from '../assets/doc_placeholder.jpg';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  align-items: center;
  p {
    position: absolute;
    bottom: -26px;
    margin: 0;
    font-size: 12px;
    color: #e91e63;
  }
`;
const Upload = styled.div<{ hasError?: any }>`
  position: relative;
  overflow: hidden;
  width: 114px;
  border-radius: 50%;
  margin-left: 15px;
  margin-top: 20px;
  height: 114px;
  background: #ecedf6;
  border: ${props =>
    props.hasError ? ' 1px dashed #e91e63' : ' 1px dashed #8886a5'};
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  input {
    position: absolute;
    width: 100%;
    height: 100%;
    opacity: 0;
    cursor: pointer;
    z-index: 2;
  }
  svg {
    font-size: 25px;
    position: absolute;
  }
  p {
    position: absolute;
    bottom: -50px;
    margin-bottom: 0;
  }
  img {
    object-fit: cover;
    width: 150%;
    z-index: 1;
  }
`;

const InputContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 20px;
`;

interface Props {
  url?: string;
  setFieldValue: (a: string, b: string) => void;
  setUrl?: (a: any) => void;
  name?: string;
  touched?: any;
  error?: string;
}
export const InputUpload = ({
  url = '',
  setUrl,
  setFieldValue,
  name = 'avatar_id',
  touched,
  error,
}: Props) => {
  const handleUpload = React.useCallback(
    async (e: React.ChangeEvent<HTMLInputElement>) => {
      const suportedTypes = [
        'image/jpeg',
        'image/png',
        'application/pdf',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      ];
      const formData = new FormData();
      // const config = {
      //   onUploadProgress(progressEvent: any) {
      //     const percentCompleted = Math.round(
      //       (progressEvent.loaded * 100) / progressEvent.total
      //     );
      //     console.log(percentCompleted);
      //   },
      // };

      formData.append(
        'operations',
        `{"query":"mutation UploadAvatarMutation($input: UploadAvatarInput!) { uploadAvatar(input: $input) {avatar {id _id original_name path url created_at updated_at}}}","variables":{"input":{"avatar":null}}}`
      );

      formData.append('map', '{"input.avatar": ["variables.input.avatar"]}');

      if (e.target.files) {
        const file = e.target?.files[0];
        const isDoc =
          file.type === 'application/pdf' ||
          file.type ===
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document';

        formData.append('input.avatar', file);
        if (!suportedTypes.includes(file?.type)) {
          return message.error(`formato não suportado`);
        }
        const createPreview = URL.createObjectURL(file);
        if (setUrl) {
          setUrl(createPreview);
        }
        if (isDoc && setUrl) {
          setUrl(docDefault);
        }
      }

      const response = await axios.post(
        'http://localhost:8000/graphql',
        formData,
        {
          // ...config,
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        }
      );

      setFieldValue(name, response?.data?.data?.uploadAvatar?.avatar._id);

      return true;
    },
    [setFieldValue, setUrl, name]
  );
  return (
    <Container>
      <InputContainer>
        <Upload hasError={touched && error}>
          <input type="file" onChange={handleUpload} />
          <FiUploadCloud />
          <img src={url} alt="" />
        </Upload>
      </InputContainer>
      <p>{touched && error}</p>
    </Container>
  );
};
