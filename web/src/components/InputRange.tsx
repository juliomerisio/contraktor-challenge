import React from 'react';

import styled from 'styled-components';
import { DatePicker } from 'antd';

const Container = styled.div<{ hasError?: boolean }>`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-top: 15px;
  .ant-picker:hover,
  .ant-picker-focused {
    border-color: transparent;
    box-shadow: none;
  }
  .ant-picker {
    border-color: ${props => props.hasError && '#e91e63'};
  }
`;
const Label = styled.label`
  margin-bottom: 4px;
  display: flex;
  p {
    margin-left: 10px;
    margin-bottom: 0;
    font-size: 12px;
    color: #e91e63;
  }
`;
const { RangePicker } = DatePicker;
const MyRangePicker = styled(RangePicker)`
  height: 40px;
  width: 100%;
  background: #ecedf6;
  border: 1px solid #e3e3f2;
  color: #131340;
  border-radius: 4px;

  * {
    background: #ecedf6;
  }
  input::placeholder {
    color: #131340;
  }
  svg {
    color: #131340;
  }
`;
interface Props {
  setFieldValue: (a: string, b: string[]) => void;
  name: string;
  label: string;
  touched: any;
  error?: any;
  value?: any;
}

export const InputRange = ({
  setFieldValue,
  name,
  label,
  touched,
  error,
  value,
}: Props) => {
  return (
    <Container hasError={touched && error}>
      <Label>
        {label}
        <p>{touched && error}</p>
      </Label>
      <MyRangePicker
        name={name}
        onChange={date => {
          return setFieldValue(name, date as any);
        }}
        allowClear
        value={value}
        placeholder={['Data de Início', 'Data de Termino']}
      />
    </Container>
  );
};
