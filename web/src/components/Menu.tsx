import React from 'react';

import styled from 'styled-components';
import { FiUsers, FiFileText, FiHome } from 'react-icons/fi';
import { Link } from 'react-router-dom';

const Container = styled.div`
  max-width: 283px;
  width: 100%;
  height: 100vh;
  border-right: 1px solid #e3e3f2;
  background: #fff;
  padding: 25px;
  position: sticky;
  top: 0;
`;
const Wrapper = styled.div`
  padding-top: 200px;
`;
const MenuItem = styled(Link)`
  display: flex;
  align-items: center;
  text-decoration: none;
  color: #8886a5;
  font-weight: bold;
  padding: 10px 0;
  p {
    margin-left: 16px;
    font-size: 16px;
    margin-bottom: 0;
  }
  svg {
    color: #716e92;
  }
  &:hover svg {
    color: #577bf9;
  }
  &:active svg {
    color: #577bf9;
  }
  &:hover {
    color: #577bf9;
  }
`;

const Menu: React.FC = () => {
  return (
    <Container>
      <Wrapper>
        <MenuItem to="/">
          <FiHome size={24} />
          <p>Home</p>
        </MenuItem>

        <MenuItem to="/pessoas">
          <FiUsers size={24} />
          <p>Pessoas</p>
        </MenuItem>

        <MenuItem to="/contratos">
          <FiFileText size={24} />
          <p>Contratos</p>
        </MenuItem>
      </Wrapper>
    </Container>
  );
};

export default Menu;
