import React from 'react';

import styled from 'styled-components';
import { FiPlus } from 'react-icons/fi';

const Container = styled.div<{ height: number }>`
  height: ${props => `${props.height}px`};
  background: #ffffff;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border-radius: 4px;
  max-width: 267px;
  width: 100%;
  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1) 0s;
  border: 1px dashed #131340;
  cursor: pointer;
`;
const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  strong {
    margin-bottom: 6px;
  }
`;
const Avatar = styled.div`
  height: 114px;
  width: 114px;
  border-radius: 50%;
  margin: 23px 0;
  border: 1px dashed #131340;
  display: flex;
  align-items: center;
  justify-content: center;
  svg {
    color: #131340;
  }
`;
const Footer = styled.div`
  min-height: 47px;
  width: 100%;
  background: #fbfbfd;
  border-top: 1px dashed #131340;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
`;

interface Props {
  onClick: VoidFunction;
}

const InputCard = ({ onClick }: Props) => {
  return (
    <Container height={311} onClick={onClick}>
      <Content>
        <Avatar>
          <FiPlus size={28} />
        </Avatar>
        <strong>Nome do usuário</strong>
        <small>example@email.com</small>
      </Content>
      <Footer />
    </Container>
  );
};

export default InputCard;
