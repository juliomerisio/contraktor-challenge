import React from 'react';
import styled from 'styled-components';

const MyButton = styled.button`
  border: none;
  background: #577bf9;
  border-radius: 4px;
  color: #fff;
  font-weight: bold;
  font-size: 14px;
  box-shadow: 0px 5px 8px rgba(165, 185, 255, 0.49);
  padding: 11px 42px;
  cursor: pointer;
  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1) 0s;

  will-change: transform;

  &:active {
    box-shadow: 0px 1px 8px rgba(165, 185, 255, 0.49);
    transform: scale(0.98);
  }
`;
export const Button = ({ children, onClick }: Props) => (
  <MyButton type="submit" onClick={onClick}>
    {children}
  </MyButton>
);

interface Props {
  children: React.ReactChild;
  onClick?: VoidFunction;
}
