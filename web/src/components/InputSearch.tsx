import React from 'react';
import styled from 'styled-components';
import { FiSearch } from 'react-icons/fi';

const Container = styled.div`
  max-width: 379px;
  width: 100%;
  display: flex;
  height: 40px;
  align-items: center;
  background: #ecedf6;
  border: 1px solid #e3e3f2;
  padding: 0 20px;
  color: #131340;
  border-radius: 4px;
  input {
    padding: 3px 10px;
    background: transparent;
    border: none;
    width: 100%;
  }
`;

interface Props {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}
const InputSearch = ({ onChange }: Props) => {
  return (
    <Container>
      <input placeholder="Procure pelo nome do usuário" onChange={onChange} />
      <FiSearch />
    </Container>
  );
};

export default InputSearch;
