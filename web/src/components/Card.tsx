import React from 'react';

import styled from 'styled-components';
import { FiDelete } from 'react-icons/fi';

const Container = styled.div<{ height: number; isActive: boolean }>`
  height: ${props => `${props.height}px`};
  background: ${props => (props.isActive ? 'rgba(87, 123, 249, 0.4)' : '#fff')};
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border-radius: 4px;
  max-width: 267px;
  width: 100%;
  box-shadow: 0px 1px 7px rgba(93, 114, 189, 0.49);
  border: ${props =>
    props.isActive ? '3px solid #577bf9' : '3px solid transparent'};
  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1) 0s;
`;
const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  word-break: break-all;
  strong {
    margin-bottom: 6px;
  }

`;
const Avatar = styled.img`
  height: 114px;
  width: 114px;
  border-radius: 50%;
  margin: 23px 0;
  object-fit: cover;
  background: transparent;
`;
const Footer = styled.div<{ isActive: boolean }>`
  min-height: 15%;
  width: 100%;
  background: ${props => (props.isActive ? '#8597d3' : '#fbfbfd')};
  border-top: ${props =>
    props.isActive ? ' 1px solid #727bc9' : ' 1px solid #ecedf6'};
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1) 0s;
`;

const Option = styled.div`
  position: absolute;
  cursor: pointer;
  border-radius: 50%;
  padding: 5px;
  top: 5px;
  right: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
  svg {
    color: #fe6161;
  }
`;

interface Props {
  height?: number;
  allowSelect?: boolean;
  allowDelete?: boolean;
  onAdd?: () => void;
  onRemove?: () => void;
  data?: any;
  parts?: any;
}

const Card = ({
  height = 311,
  onAdd,
  onRemove,
  allowDelete = false,
  data,
  parts,
}: Props) => {
  const isActive = parts?.find((el: any) => el.id === data.id);

  const defaultAvatar = `https://avataaars.io/?avatarStyle=Transparent&topType=LongHairShavedSides&accessoriesType=Kurt&facialHairType=BeardMedium&facialHairColor=Blonde&clotheType=Overall&clotheColor=Heather&eyeType=Side&eyebrowType=Default&mouthType=Tongue&skinColor=Pale`

  return (
    <Container
      height={height}
      onClick={() => {
        return onAdd && onAdd();
      }}
      isActive={isActive}
    >
      <Content>
        <Avatar src={data?.avatar?.url || defaultAvatar} />
        <strong>{data?.name}</strong>
        <small>{data?.email}</small>
      </Content>

      <Footer isActive={isActive} />

      {allowDelete && (
        <Option
          onClick={() => {
            return onRemove && onRemove();
          }}
        >
          <FiDelete size={22} />
        </Option>
      )}
    </Container>
  );
};

export default Card;
