import React, { ChangeEvent } from 'react';
import styled from 'styled-components';

const Container = styled.div<{ isError: boolean }>`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-bottom: 15px;
  input {
    padding: 3px 10px;
    height: 40px;
    background: #ecedf6;
    transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1) 0s;
    border: ${props =>
      props.isError ? '1px solid #e91e63' : '1px solid #e3e3f2'};
    color: #131340;
    border-radius: 4px;
  }
`;
const Label = styled.label`
  margin-bottom: 4px;
  display: flex;
  p {
    margin-left: 10px;
    margin-bottom: 0;
    font-size: 12px;
    color: #e91e63;
  }
`;

export function cpf(e: React.FormEvent<HTMLInputElement>) {
  e.currentTarget.maxLength = 14;
  let { value } = e.currentTarget;
  if (!value.match(/^(\d{3}).(\d{3}).(\d{3})-(\d{2})$/)) {
    value = value.replace(/\D/g, '');
    value = value.replace(/(\d{3})(\d)/, '$1.$2');
    value = value.replace(/(\d{3})(\d)/, '$1.$2');
    value = value.replace(/(\d{3})(\d{2})$/, '$1-$2');
    e.currentTarget.value = value;
  }
  return e;
}
export function phone(e: React.FormEvent<HTMLInputElement>) {
  e.currentTarget.maxLength = 13;

  let { value } = e.currentTarget;
  if (!value.match(/^(\d{2})-(\d{6})-(\d{3})-(\d{4})$/)) {
    value = value.replace(/\D/g, '');
    value = value.replace(/(\d{2})(\d)/, '$1 $2');
    value = value.replace(/(\d{5})(\d)/, '$1-$2');
    e.currentTarget.value = value;
  }
  return e;
}

const Input = ({
  name,
  label,
  value,
  onChange,
  touched,
  error,
  validation,
}: Props) => {
  const handleKeyUp = React.useCallback(
    (e: React.FormEvent<HTMLInputElement>) => {
      if (!validation) {
        return;
      }
      if (validation === 'cpf') {
        cpf(e);
      }
      if (validation === 'phone') {
        phone(e);
      }
    },
    [validation]
  );
  return (
    <Container isError={touched && error}>
      <Label>
        {label}
        <p>{touched && error}</p>
      </Label>

      <input
        name={name}
        onChange={onChange}
        value={value}
        onKeyUp={handleKeyUp}
      />
    </Container>
  );
};

interface Props {
  name: string;
  label: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  value: string;
  error: any;
  touched: any;
  validation?: any;
}

export default Input;
