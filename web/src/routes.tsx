import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import NotFound from './pages/404';
import ManageContracts from './pages/Contract/Manage';
import ListContract from './pages/Contract/List';
import ManageUsers from './pages/User/Manage';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/contratos" exact component={ManageContracts} />
      <Route path="/contratos/editar/:id" exact component={ManageContracts} />
      <Route path="/pessoas" exact component={ManageUsers} />
      <Route path="/pessoas/:id" exact component={ManageUsers} />
      <Route path="/contratos/:id" exact component={ListContract} />
      <Route path="*" exact component={NotFound} />
    </Switch>
  );
}
