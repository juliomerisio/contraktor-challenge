import { createGlobalStyle } from 'styled-components';
import 'antd/dist/antd.css';

export default createGlobalStyle`

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }
  html {
    font-size: 62.5%;
  }

  body {
    background: #F6F6FA;
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialiased !important;

  }
  body, input, textarea {
    font-family: 'Open Sans', sans-serif;
    font-size: 16px;
  }
  a, button {
    outline: none;
  }


  /* Font Sizes */
  h1 {
    font-weight: bold;
    font-size: 3rem;
    color: #131340;
  }
  strong {
    color: #131340;
    font-size: 18px;
  }
  small, label {
    font-size: 14px;
    line-height: 16px;
    color: #8886A5;
  }

`;
