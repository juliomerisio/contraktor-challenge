import React from 'react';
import { Router } from 'react-router-dom';
import Providers from './Providers';
import Routes from './routes';
import history from './services/history';
import GlobalStyle from './styles/global';

function App() {
  return (
    <Providers>
      <Router history={history}>
        <Routes />
        <GlobalStyle />
      </Router>
    </Providers>
  );
}

export default App;
