import React from 'react';
import styled from 'styled-components';
import Menu from '../components/Menu';

const Container = styled.div`
  display: flex;
  position: relative;
`;
interface Props {
  children: React.ReactChild;
}

const Layout = ({ children }: Props) => {
  return (
    <Container>
      <Menu />
      {children}
    </Container>
  );
};
export default Layout;
