
<h1 align="center">
Contraktor - Challenge ⚔️
</h1>

<p align="center">
  <a href="https://contraktor.com.br/">
    <img src="https://thumbs.jusbr.com/filters:format(webp)/imgs.jusbr.com/publications/images/39d66280b521b36ae6165297983b4188" alt="Banner">
  </a>
</p>

<hr>

## 📄 Requisitos do desafio

-  Cadastrar, consultar, listar e excluir um contrato.
-  No cadastro do contrato considerar o título, as datas de inicio e vencimento e o arquivo em PDF/DOC do contrato.
-  Cadastrar, consultar e listar uma parte.
-  No cadastro da parte considerar nome, sobrenome, email, CPF e telefone.
-  Relacionar um contrato à uma ou mais partes.
-  Visualização simples de um contrato com as partes relacionadas ao contrato.

## ✋🏻 Pré-requisitos

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/pt-BR/docs/install)
- [Docker](https://www.docker.com/get-started) 🐳
- [Docker Compose](https://docs.docker.com/compose/install/)
  
## 🔥Instalação e execução

### Api

0. Clone este repositório através do comando `git clone https://gitlab.com/juliomerisio/contraktor-challenge.git`<br />
1. Entre na pasta do projeto `cd contraktor-challenge`<br />
2. Execute `yarn install` na pasta api para instalar as dependências.<br />
3. Execute `docker-compose up -d ` para iniciar o container com o postgres.<br />
4. Execute `gen-schema-types` para gerar os types com o - [GraphQL Codegen](https://graphql-code-generator.com/).<br /> 
5. Execute `yarn tsc` para compilar o código<br />
6. Execute `yarn sequelize db:create challenge` para criar a database com nome challenge dentro do container.<br />
7. Execute `yarn sequelize db:migrate` para iniciar as migrations<br />
8.  Execute `yarn dev` para iniciar o projeto<br />

### Web
1. Entre na pasta web;<br />
2. Execute `yarn install` na pasta web para instalar as dependências.<br />
3. Execute `yarn start` para iniciar o projeto<br />

- ⚠️ Caso necessite voltar as migrations execute `yarn sequelize db:migrate:undo:all` <br/>

## Demo
![README/demo.fig](README/demo.gif)