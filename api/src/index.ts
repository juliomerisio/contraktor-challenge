import { errorHandler } from 'graphql-api-koa';
import { router } from './routes';
import { errorMiddleware } from './middleware/errorMiddleware';

const cors = require('@koa/cors');

import Bodyparser = require('koa-bodyparser');

import Koa = require('koa');

const app = new Koa();
app.use(cors());
app.use(Bodyparser());
app.use(errorHandler());
app.use(errorMiddleware);
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(8000, () => {
  console.log('server is running on http://localhost:8000/playground'); //eslint-disable-line
});
