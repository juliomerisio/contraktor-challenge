import { models } from '../db';
import { getLoaders } from './dataloaders';

export function getContext() {
  return {
    models,
    dataloaders: getLoaders(models),
  };
}

export type GraphQLContext = ReturnType<typeof getContext>;
