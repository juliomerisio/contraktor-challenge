import { toGlobalId } from 'graphql-relay';
import { Op } from 'sequelize';
import { GQLResolvers } from '../../generated/schema';

const resolvers: GQLResolvers = {
  User: {
    id({ id }) {
      return toGlobalId('User', id);
    },
    _id({ id }) {
      return id;
    },
    avatar({ avatar_id }, _args, ctx) {
      return avatar_id ? ctx.dataloaders.Avatar.load(avatar_id) : null;
    },
  },
  Mutation: {
    async createUser(_parent, args, ctx) {
      const { values, clientMutationId } = args.input;
      const userExists = await ctx.models.User.findOne({
        where: {
          email: values.email,
        },
      });
      if (userExists) {
        return {
          Error: ['Email está em uso'],
          clientMutationId,
        };
      }
      const user = await ctx.models.User.create(values);

      return {
        user,
        clientMutationId,
      };
    },
    async updateUser(_parent, args, ctx) {
      const { values, clientMutationId, id } = args.input;
      const user = await ctx.models.User.findByPk(id);

      if (!user) {
        return {
          clientMutationId,
          Error: ['user not found'],
        };
      }

      await user.update(values);

      return {
        user,
        clientMutationId,
      };
    },
  },
  Query: {
    async users(_parent, _args, ctx) {
      let query = {};

      if (_args.q) {
        query = {
          where: {
            name: { [Op.like]: `${_args.q}%` },
          },
        };
      }

      const users = await ctx.models.User.findAll(query);
      return users;
    },
  },
};

export default resolvers;
