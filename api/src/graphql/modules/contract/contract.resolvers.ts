import { toGlobalId } from 'graphql-relay';
import { Op } from 'sequelize';
import { GQLResolvers } from '../../generated/schema';
import { without } from 'ramda';

const resolvers: GQLResolvers = {
  Mutation: {
    async createContract(_parent, args, ctx) {
      const { values, clientMutationId } = args.input;
      const { user_id, ...cleanValues } = values;

      const users = await ctx.models.User.findAll({
        where: {
          id: {
            [Op.in]: user_id,
          },
        },
      });

      const [contract] = await ctx.models.Contract.findOrCreate({
        where: { ...cleanValues },
      });


      // @ts-ignore
      users.forEach(async (user) => user.addContract(contract));

      return {
        contract,
        clientMutationId,
      };
    },
    async updateContract(_parent, args, ctx) {
      const { values, clientMutationId, id } = args.input;
      const { user_id, ...cleanValues } = values;

      const newUsers = await ctx.models.User.findAll({
        where: {
          id: {
            [Op.in]: user_id,
          },
        },
      });
      // @ts-ignore
      const { users } = await ctx.models.Contract.findByPk(id, {
        // @ts-ignore
        include: { association: 'users' },
      });

      const contract = await ctx.models.Contract.findByPk(id);





      if (!contract) {
        return {
          clientMutationId,
          Error: ['contract not found'],
        };
      }

      const removedUsersFromContract =  without(newUsers, users)

      // @ts-ignore
      removedUsersFromContract.map(async (user) => await user.removeContract(contract));

      // @ts-ignore
      newUsers.map(async (user) => await user.addContract(contract));


      await contract.update(cleanValues);

      return {
        contract,
        clientMutationId,
      };
    },
    async deleteContract(_parent, args, ctx) {
      const { id } = args.input;
      const contract = await ctx.models.Contract.findByPk(id);

      if (!contract) {
        return {
          Error: ['contract not found'],
        };
      }
      await contract.destroy();
      return {
        message: 'contrato deletado com sucesso',
      };
    },
  },
  Contract: {
    _id({ id }) {
      return id;
    },
    id({ id }) {
      return toGlobalId('Contract', id);
    },
    document: ({ document_id }, _args, ctx) => {
      return ctx.dataloaders.Avatar.load(document_id);
    },
    parts: async (_parent, _args, ctx) => {
      // @ts-ignore
      const { users } = await ctx.models.Contract.findByPk(_parent.id, {
        // @ts-ignore
        include: { association: 'users' },
      });
      return users;
    },
  },
  Query: {
    async contracts(_parent, _args, ctx) {
      let query = {};

      if (_args.q) {
        query = {
          where: {
            name: { [Op.like]: `%${_args.q}` },
          },
        };
      }

      const contracts = await ctx.models.Contract.findAll(query);
      return contracts;
    },
  },
};

export default resolvers;
