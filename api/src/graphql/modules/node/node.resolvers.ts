import { fromGlobalId } from 'graphql-relay';
import { GQLResolvers } from '../../generated/schema';
import { Models } from '../../../db';
import { GraphQLContext } from '../../context';

function isSupportedType(
  type: string,
  ctx: GraphQLContext
): type is keyof Models {
  return type in ctx.models;
}

const resolvers: GQLResolvers = {
  Query: {
    async node(_parent, args, ctx): Promise<any> {
      const { type, id } = fromGlobalId(args.id);

      if (!isSupportedType(type, ctx)) {
        return null;
      }
      const loader = ctx.dataloaders[type];
      const node = await loader.load(id);

      if (!node) {
        return null;
      }

      return {
        ...node,
        __typename: type,
      };
    },
  },
  Node: {
    __resolveType: ({ __typename }: any) => __typename,
  },
};

export default resolvers;
