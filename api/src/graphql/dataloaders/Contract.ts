import { Op } from 'sequelize';
import { Models } from '../../db';

import Dataloader = require('dataloader');

export function getLoader(models: Models) {
  return new Dataloader(async (ids: readonly string[]) => {
    const contracts = await models.Contract.findAll({
      where: {
        id: {
          [Op.in]: ids as string[],
        },
      },
      raw: true,
    });
    return ids.map(
      (id) => contracts.find((contract) => contract.id === id) || null
    );
  });
}
