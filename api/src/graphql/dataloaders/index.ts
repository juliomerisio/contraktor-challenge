import { getLoader as getContractLoader } from './Contract';
import { getLoader as getUserLoader } from './User';
import { getLoader as getAvatarLoader } from './Avatar';
import { Models } from '../../db';

export function getLoaders(models: Models) {
  return {
    User: getUserLoader(models),
    Contract: getContractLoader(models),
    Avatar: getAvatarLoader(models),
  };
}
