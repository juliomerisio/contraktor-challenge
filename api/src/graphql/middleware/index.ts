import { validatorsMiddleware } from './validation';
import { mutationMiddleware } from './mutation';

export const middlewares = [validatorsMiddleware, mutationMiddleware];
