import { Sequelize, Model, BuildOptions } from 'sequelize';
import * as config from './config';
import * as User from './models/User';
import * as Contract from './models/Contract';
import * as Avatar from './models/Avatar';

function isNodeEnvValid(env?: string): env is keyof typeof config {
  return !!env && env in config;
}

const env = process.env.NODE_ENV;
if (!isNodeEnvValid(env)) {
  throw new Error('invalid environment');
}
const seqConfig = config[env];

export const sequelize = new Sequelize(seqConfig);

function buildModel(seq: Sequelize) {
  const models = {
    User: User.build(seq),
    Contract: Contract.build(seq),
    Avatar: Avatar.build(seq),
  };

  Object.keys(models).forEach((key) => {
    const modelKey = key as keyof typeof models;
    if (models[modelKey].associate) {
      models[modelKey].associate!(models);
    }
  });
  return models;
}

export const models = buildModel(sequelize);

export type Models = ReturnType<typeof buildModel>;

type AvailableModelKeys = keyof Models;
type AvailableModels = Models[AvailableModelKeys];

// @ts-ignore
type SequelizeInstanceType<TStatic> = TStatic extends ?(typeof Model & {
  new (values?: Partial<infer U>, options?: BuildOptions): infer U;
})
  ? U
  : never;

export type AvailableModelsInstaceTypes = SequelizeInstanceType<
  AvailableModels
>;

export type SequelizeStaticType<TInstance> = typeof Model & {
  new (values?: Partial<TInstance>, options?: BuildOptions): TInstance;
} & {
  associate?: (
    models: Record<
      string,
      typeof Model & {
        new (values?: Partial<any>, options?: BuildOptions): any;
      }
    >
  ) => void;
};

export type AnyModel = SequelizeStaticType<AvailableModelsInstaceTypes>;
