import { Model, Sequelize, DataTypes } from 'sequelize';
import { SequelizeStaticType } from '..';

export interface Contract extends Model {
  readonly id: string;
  title: string;
  document_id: string;
  user_id: string;
  validityStart: string;
  validityEnd: string;
  created_at: Date;
  updated_at: Date;
}

type ContractStatic = SequelizeStaticType<Contract>;

export function build(sequelize: Sequelize) {
  const Contract = sequelize.define(
    'contract',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      document_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'avatars',
          key: 'id',
        },
        onDelete: 'SET NULL',
        onUpdate: 'CASCADE',
      },
      validityStart: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      validityEnd: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  ) as ContractStatic;

  Contract.associate = (models) => {
    models.Contract.belongsTo(models.Avatar, {
      foreignKey: 'document_id',
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    });
  };

  Contract.associate = (models) => {
    models.Contract.belongsToMany(models.User, {
      foreignKey: 'contract_id',
      through: 'user_contracts',
      as: 'users',
    });
  };

  return Contract;
}
