import { Model, Sequelize, DataTypes } from 'sequelize';
import { SequelizeStaticType } from '..';

export interface User extends Model {
  readonly id: string;
  name: string;
  email: string;
  avatar_id: string | null;
  password_hash: string;
  created_at: Date;
  updated_at: Date;
}

type UserStatic = SequelizeStaticType<User>;

export function build(sequelize: Sequelize) {
  const User = sequelize.define(
    'user',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      avatar_id: {
        type: DataTypes.UUID,
        references: {
          model: 'avatars',
          key: 'id',
        },
        onDelete: 'SET NULL',
        onUpdate: 'CASCADE',
      },
      surname: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      cpf: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  ) as UserStatic;

  User.associate = (models) => {
    models.User.belongsTo(models.Avatar, {
      foreignKey: 'avatar_id',
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    });
  };

  User.associate = (models) => {
    models.User.belongsToMany(models.Contract, {
      foreignKey: 'user_id',
      through: 'user_contracts',
      as: 'contracts',
    });
  };

  return User;
}
