import { Model, Sequelize, DataTypes } from 'sequelize';
import { SequelizeStaticType } from '..';

export interface Avatar extends Model {
  readonly id: string;
  originalName: string;
  path: string;
  created_at: Date;
  updated_at: Date;
}

type AvatarStatic = SequelizeStaticType<Avatar>;

export function build(sequelize: Sequelize) {
  const Avatar = sequelize.define(
    'avatar',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      originalName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      path: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  ) as AvatarStatic;
  return Avatar;
}
