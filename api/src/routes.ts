import { execute } from 'graphql-api-koa';
import Playground from 'graphql-playground-middleware-koa';
import { graphqlUploadKoa } from 'graphql-upload';
import { resolve } from 'path';
import { schema } from './graphql';
import { getContext } from './graphql/context';

import send = require('koa-send');

import Router = require('@koa/router');

const router = new Router();

router.get('/hello', (ctx, next) => {
  ctx.body = 'hello visitor';

  return next();
});

router.get('/uploads/:file', (ctx) => {
  return send(ctx, ctx.params.file, {
    root: resolve(__dirname, '..', 'tmp', 'uploads'),
  });
});

router.all('/playground', Playground({ endpoint: '/graphql' }));
router.post(
  '/graphql',
  graphqlUploadKoa({
    maxFiles: 10,
    maxFileSize: 10000000,
  }),
  execute({
    override: () => {
      return {
        schema,
        contextValue: getContext(),
      };
    },
  })
);

export { router };
